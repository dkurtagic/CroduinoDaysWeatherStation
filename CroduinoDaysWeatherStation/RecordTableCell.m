//
//  RecordTableCellTableViewCell.m
//  CroduinoDaysWeatherStation
//
//  Created by Dino Kurtagic on 24/03/15.
//  Copyright (c) 2015 Dino Kurtagic. All rights reserved.
//

#import "RecordTableCell.h"

@implementation RecordTableCell

- (void)setItem:(WeatherRecord *)item
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyyy"];
    formatter.timeZone = [NSTimeZone localTimeZone];
    NSString *dateString = [formatter stringFromDate:item.date];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSString *timeString = [formatter stringFromDate:item.date];
    _dateLabel.text = [NSString stringWithFormat:@"%@\n%@",dateString, timeString];
    
    _tempLabel.text = [NSString stringWithFormat:@"Temperatura: %@",item.temp.stringValue];
    _humLabel.text = [NSString stringWithFormat:@"Vlaga: %@", item.hum.stringValue];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
