//
//  ViewController.m
//  CroduinoDaysWeatherStation
//
//  Created by Dino Kurtagic on 23/03/15.
//  Copyright (c) 2015 Dino Kurtagic. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "WeatherRecord.h"
#import "RecordTableCell.h"

#define TABLE_CELL_IDENTIFIER @"recordcell"

@interface ViewController ()
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (weak, nonatomic) IBOutlet UITableView *recordsTableView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@end

@implementation ViewController

#pragma mark - view load
- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArray = [NSMutableArray new];
    
    [_recordsTableView registerNib:[UINib nibWithNibName:@"RecordTableCell"
                                              bundle:[NSBundle mainBundle]]
        forCellReuseIdentifier:TABLE_CELL_IDENTIFIER];
    
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor purpleColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(loadData)
                  forControlEvents:UIControlEventValueChanged];
    [self.recordsTableView addSubview:self.refreshControl];
}

#pragma mark - data load
- (void)loadData
{
    __weak ViewController *weakself = self;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = @"http://lycheesys.com/api/croduinodays/json.php";
    [_dataArray removeAllObjects];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if([responseObject isKindOfClass:[NSArray class]])
        {
            for (NSDictionary *dict in responseObject)
            {
                if([dict isKindOfClass:[NSDictionary class]])
                {
                    [weakself.dataArray addObject:[[WeatherRecord alloc]initWithDictionary:dict]];
                }
            }
            [weakself.recordsTableView reloadData];
            
            // End the refreshing
            if (weakself.refreshControl) {
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MMM d, h:mm a"];
                NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
                NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                            forKey:NSForegroundColorAttributeName];
                NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
                self.refreshControl.attributedTitle = attributedTitle;
                
                [self.refreshControl endRefreshing];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Greška" message:[error localizedDescription] delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alertView show];
        
        [weakself.refreshControl endRefreshing];
    }];
}

#pragma mark - table view delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RecordTableCell *cell = [tableView dequeueReusableCellWithIdentifier:TABLE_CELL_IDENTIFIER];
    
    [cell setItem:[_dataArray objectAtIndex:indexPath.row]];
    return cell;

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (_dataArray.count > 0) {
        
        self.recordsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        return 1;
        
    } else {
        
        // Display a message when the table is empty
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"Trenutno nema podataka. Molimo učitajte podatke .";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:18];
        [messageLabel sizeToFit];
        
        self.recordsTableView.backgroundView = messageLabel;
        self.recordsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return 0;
}

#pragma mark - memory handling
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
