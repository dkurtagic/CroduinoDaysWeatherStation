//
//  RecordTableCellTableViewCell.h
//  CroduinoDaysWeatherStation
//
//  Created by Dino Kurtagic on 24/03/15.
//  Copyright (c) 2015 Dino Kurtagic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherRecord.h"
@interface RecordTableCell : UITableViewCell
{
    __weak IBOutlet UILabel *_dateLabel;
    __weak IBOutlet UILabel *_tempLabel;
    __weak IBOutlet UILabel *_humLabel;

}

@property (nonatomic, strong) WeatherRecord *item;

@end
